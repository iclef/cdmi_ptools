#!/usr/bin/python
# Copyright(c) 2013 Hirotaka Igarashi.  Allrights reserved.
#

import getopt,sys
import cdmicommon
import urllib

def show_url( url, cdmi_object ):
    print(urllib.url2pathname(url))

def main():
    url = None
    passwd = None
    try:
        opts, args = getopt.gnu_getopt(sys.argv[1:], "p:", ["pass="])
    except getopt.GetoptError, err:
        print str(err)
        usage()
        sys.exit(1)
    for o,a in opts:
        if o in ("-p", "--pass"):
            passwd = a
        else:
            assert False, "unhandled option"
    if len(args) == 0:
        usage()
        sys.exit(1)
    url = args[0]
    if passwd is None:
        print( "Error: missing password" )
        usage()
        sys.exit(1)
    cdmicommon.initAuthHandler(url,passwd)
    cdmicommon.digChild(url, show_url, '', ('children',))

def usage():
    print("storage_used -p passwd serverurl")
    print("  ie. storage_used -p mypass http://localhost:81")
    

if __name__=="__main__":
    main()

