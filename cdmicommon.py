# coding: utf-8

import urllib,urllib2
import json
import getopt,sys


cdmi_headers = { 'Accept':'*/*',
                 'X-CDMI-Specification-Version':'1.0' }

def initAuthHandler(url, pwd):
    ah = urllib2.HTTPBasicAuthHandler()
    ah.add_password(realm='default', uri=url,
                    user='administrator',passwd=pwd)
    opener = urllib2.build_opener(ah)
    urllib2.install_opener(opener)
    

def digChild( parentURL, func, child='', elements=() ):
    """digging cdmi tree under child."""
    """On each node function(url, cdmi_object) is invoked."""
    """child is used to revursive call."""
    """elements is tuple which contains queried parameters, such as children, metadata..."""
    url = parentURL
    child = urllib.pathname2url( child.encode('UTF-8') )
    # TODO: escaping '/' characters?
    # child = child.replace('/','\/');
    if url.endswith('/'): url+=child
    else: url+='/'+child
    # parameters ?xxx;yyy;... 
    params=serializeParameters(elements)
    # i got canonicalized url here
    # debug__
    # print(url+params)
    # __debug
    req = urllib2.Request(url+params, headers=cdmi_headers)
    fp = urllib2.urlopen(req)
    co = json.load(fp)
    # invokes business logic
    func( url, co )
    # dig more deeper
    if co.has_key('children'):
        children = co['children'];
        for c in children:
            digChild(url, func, c, elements )

def serializeParameters(elements):
    """ accept cdmi query parameters in tuple and serialize it to url query string """
    """ ie. serializedParameters( ('children',) ) """
    params = ''
    if len(elements)>0:
        params='?'
        i = 0;
        while i < len(elements):
            params+=elements[i]
            i+=1
            if i!=len(elements):
                params+=';'
    return params


